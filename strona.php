
<?php 

$strona = array(
		'home' => array(
				'title' 	=> 'Strona domowa',
				'content' 	=> 'Witaj na mojej stronie przybyszu z intenetów!'
			),
		'about' => array(
				'title' 	=> 'O mnie',
				'content' 	=> 'Jestem początkującym programistą ale staram się być coraz lepszy w tym co robię!'
			),
		'contact' => array(
				'title' 	=> 'Kontakt',
				'content' 	=> 'Aby się ze mną skontaktować napisz do mnie maila na adres {moj-adres}'
			),
		'sites' => array(
				'title'		=> 'Przydatne strony',
				'content' 	=> '<a href="http://php.net/">Dokumentacja PHP</a> <br> <a href="http://stackoverflow.com/">Kto pyta nie błądzi</a>'
			)
	);

	
		?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Strona</title>
		<meta name="viewport" content="width=device-width,initial-scale=1.0">
		<link href="css/main.css" rel="stylesheet">
		<link href="css/demo.css" rel="stylesheet">
	</head>
	<body>

		<div class="wrapper">
			<div class="main">
				
				<!-- Nav -->
				<nav class="nav">
					<ul class="nav-list">
						<?php foreach($strona as $s => $s_values){ ?>
						<li class="nav-item">
							<a href="?podstrona=<?php echo $s ?>"><?php echo $s ?></a>
						</li>
						<?php } ?>
					</ul>
				</nav>
				<!-- /Nav -->
				<?php if(isset($_GET['podstrona'])){ ?>
				<div class="content">
					<?php 
						$podstrona = $_GET['podstrona'];
						echo "<h3>".$strona[$podstrona]['title']."</h3></br>";
						echo str_replace("{moj-adres}","olekiwanski@gmail.com",$strona[$podstrona]['content'])."</br>";
					?>
				</div>
				<?php }else{ ?>
				<div class="content">
				
				<?php 
						echo "<h3>".$strona['home']['title']."</h3></br>";
						echo $strona['home']['content']."</br>";
				?>
				
				</div>
				<?php } ?>
			</div>
		</div>
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="js/flaunt.js"></script>
	

		

	</body>
</html>
<!-- /Nav -->
